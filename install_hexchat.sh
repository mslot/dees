#!/usr/bin/env bash

echo 'installing hexchat with theme'
sudo dnf -y hexchat
wget -O ~Downloads/Ubuntu_Dark.hct https://dl.hexchat.net/themes/Ubuntu%20Dark.hct
mkdir -p ~/.config/hexchat
unzip -o ~/Downloads/Ubuntu_Dark.hct -d ~/.config/hexchat
