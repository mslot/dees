# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
let
  home-manager = builtins.fetchTarball
    "https://github.com/nix-community/home-manager/archive/master.tar.gz";
in {
  imports = [ # Include the results of the hardware scan.
    <nixos-hardware/microsoft/surface>
    /etc/nixos/hardware-configuration.nix

    # home-manager
    (import "${home-manager}/nixos")

    # modules
    /home/mslot/nixos/modules/zsh.nix
    /home/mslot/nixos/modules/basePackages.nix
    /home/mslot/nixos/modules/discord.nix
    /home/mslot/nixos/modules/csharp.nix
    /home/mslot/nixos/modules/emacs.nix
    /home/mslot/nixos/modules/docker.nix
    /home/mslot/nixos/modules/fonts.nix
    /home/mslot/nixos/modules/gnome.nix
    /home/mslot/nixos/modules/home.nix
    /home/mslot/nixos/modules/alacritty.nix
    /home/mslot/nixos/modules/environmentVariables.nix
    /home/mslot/nixos/modules/system.nix
    /home/mslot/nixos/modules/irssi.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "hopper";
  networking.useDHCP = false;
  networking.interfaces.wlp1s0.useDHCP = true;

  services.xserver = {
    enable = true;
    desktopManager.gnome.enable = true;
    displayManager.gdm.enable = true;
    #    videoDrivers = [ "nvidia" ];
    libinput = { enable = true; };
    layout = "dk";
    xkbOptions = "eurosign:e";
  };

}
