# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:
let
  home-manager = builtins.fetchTarball
    "https://github.com/nix-community/home-manager/archive/master.tar.gz";
in {
  imports = [ # Include the results of the hardware scan.
    /etc/nixos/hardware-configuration.nix

    # home-manager
    (import "${home-manager}/nixos")

    # modules
    /home/mslot/nixos/modules/zsh.nix
    /home/mslot/nixos/modules/basePackages.nix
    /home/mslot/nixos/modules/discord.nix
    /home/mslot/nixos/modules/csharp.nix
    /home/mslot/nixos/modules/emacs.nix
    /home/mslot/nixos/modules/docker.nix
    /home/mslot/nixos/modules/fonts.nix
    /home/mslot/nixos/modules/gnome.nix
    /home/mslot/nixos/modules/home.nix
    /home/mslot/nixos/modules/alacritty.nix
    /home/mslot/nixos/modules/environmentVariables.nix
    /home/mslot/nixos/modules/system.nix
    /home/mslot/nixos/modules/irssi.nix
    /home/mslot/nixos/modules/mouse.nix
  ];

  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  networking.hostName = "grace";
  networking.wireless.enable = false;
  networking.networkmanager.enable = true;
  networking.useDHCP = false;
  networking.interfaces.enp5s0.useDHCP = true;
  networking.interfaces.wlp6s0.useDHCP = true;

  programs.steam.enable = true;

  hardware.nvidia.modesetting.enable = true;

  services.xserver = {
    enable = true;
    layout = "dk";
    xkbOptions = "eurosign:e";
    displayManager.gdm.enable = true;
    desktopManager.gnome.enable = true;
    displayManager.setupCommands =
      "${pkgs.xorg.xrandr}/bin/xrandr --output DP-0 --mode 2560x1440 --rate 144 --primary --left-of HDMI-0 --output HDMI-0 --mode 2560x1440";
    videoDrivers = [ "nvidia" ];
    libinput = {
      enable = true;
      mouse = {
        accelSpeed = "-0.4";
        accelProfile = "flat";
      };
    };
  };
}
