{ config, lib, pkgs, ... }:

{

  environment.systemPackages = with pkgs; [ zsh-prezto starship ];

  home-manager.users.mslot = {
    home.file.".zpreztorc".text = ''
      zstyle ':prezto:load' pmodule \
        'environment' \
        'terminal' \
        'editor' \
        'history' \
        'fasd' \
        'directory' \
        'spectrum' \
        'utility' \
        'syntax-highlighting' \
        'completion' \
        'history-substring-search' \
        'prompt'

      zstyle ':prezto:module:syntax-highlighting' highlighters \
        'main' \
        'brackets' \
        'pattern' \
        'line' \
        'cursor' \
        'root'

      zstyle ':prezto:module:syntax-highlighting' color 'yes'
      zstyle ':prezto:module:editor' key-bindings 'vi'
          '';
  };

  programs.zsh = {
    enable = true;
    shellAliases = {
      config = "${pkgs.git}/bin/git --git-dir=$HOME/.dees/ --work-tree=$HOME";
      build =
        "sudo nixos-rebuild switch -I nixos-config=$HOME/nixos/hosts/$HOST/configuration.nix";
      cat = "bat";
      ls = "exa --icons --git --header --group-directories-first -l";
      tree = "ls --tree";
      du = "dust";
      cls = "clear";
      top = "btm";
      e = "emacs";
      en = "emacs -nw";
      k = "kubectl";
    };
    enableCompletion = true;
    autosuggestions.enable = true;
    interactiveShellInit = ''
      export LOCALPREZTODIR=${pkgs.zsh-prezto}
      source "$LOCALPREZTODIR/share/zsh-prezto/init.zsh"
    '';
    promptInit = ''
      eval "$(starship init zsh)"
    '';
  };
}
