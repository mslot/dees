{config, pkgs, ...}:
{
  fonts.fonts = with pkgs; [ (nerdfonts.override { fonts = [ "FiraCode" "DroidSansMono" "Iosevka" ]; }) iosevka font-awesome weather-icons ];
}
