# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

let
  buildDotnet = attrs:
    pkgs.callPackage (import (builtins.fetchurl {
      url =
        "https://raw.githubusercontent.com/NixOS/nixpkgs/nixos-21.05/pkgs/development/compilers/dotnet/build-dotnet.nix";
      sha256 =
        "458758e351c2d122f900ce6b27a472cc1b1b87a14fe2ff584416b9380eaf320d";
    }) attrs) { };
  net_6_0 = buildDotnet { # not used, but kept here for further reference
    type = "sdk";
    version = "6.0.100-preview.7.21379.14";
    sha512 = {
      x86_64-linux =
        "c8757325407b5eb1e3870f0db87eeaf44df978313c0b2a7f465ec7d0a5647317cba597264ec81577ea0b3bd97bd33d782234392e8e592e073126792a0406df7b";
    };
  };
in {
  environment.systemPackages = with pkgs; [
    (with dotnetCorePackages; combinePackages [ sdk_3_1 sdk_5_0 sdk_6_0 ])
    mono
    omnisharp-roslyn
  ];
}
