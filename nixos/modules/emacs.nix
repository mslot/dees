{ config, pkgs, ... }: {
  environment.systemPackages = with pkgs; [ emacs ];
  # services.emacs.enable = true;
  environment.variables = {
    EDITOR = "emacs";
    VISUAL = "emacs";
    ALTERNATE_EDITOR = "";
  };
}
