{ pkgs, ... }: {
  environment.systemPackages = with pkgs; [
    hexchat
    thunderbird
    jq
    neovim
    postgresql
    kubectl
    argocd
    kubernetes-helm
    fd
    teams
    jetbrains.rider
    ripgrep
    coreutils
    clang
    git
    unzip
    rust-analyzer
    rustup
    xdotool
    xclip
    pass
    gnupg
    xorg.xprop
    xorg.xwininfo
    wl-clipboard-x11
    azuredatastudio
    protobuf
    python310
    exa
    du-dust
    bottom
    tldr
    fzf
    feh
    nixfmt
    bat
    firefox
    vim
    wget
    glib
    nodePackages.typescript
    gnumake
  ];

  programs.gnupg.agent = {
    enable = true;
    pinentryFlavor = "gnome3";
    enableSSHSupport = true;
  };

}
