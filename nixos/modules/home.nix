{ config, lib, pkgs, ... }:

# all modules is defined here: https://github.com/nix-community/home-manager/tree/master/modules/programs
{
  home-manager.users.mslot = {
    programs.home-manager.enable = true;
    xsession.enable = true;
  };
}
