{ nodejs, pkgs, ... }: {
  programs.gnome-terminal.enable = true;

  environment.systemPackages = with pkgs; [
    gnomeExtensions.unite
    gnomeExtensions.pop-shell
    gnome.gnome-tweaks
  ];
}
