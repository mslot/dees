{ config, lib, pkgs, ... }:

{
  time.timeZone = "Europe/Amsterdam";

  i18n.defaultLocale = "en_US.UTF-8";

  console = {
    font = "Lat2-Terminus16";
    keyMap = "dk";
  };

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  users.users.mslot = {
    isNormalUser = true;
    extraGroups = [ "wheel" "docker" ]; # Enable ‘sudo’ for the user.
  };

  system.stateVersion = "21.05"; # Did you read the comment?

  nixpkgs.config.allowUnfree = true;
  #   hardware.nvidia.modesetting.enable = true;

  nix.gc = {
    automatic = true;
    dates = "weekly";
    options = "--delete-older-than 30d";
  };

  system.autoUpgrade.enable = true;
  system.autoUpgrade.allowReboot = true;
}
