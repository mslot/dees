{pkgs, config, ...}:
{
    virtualisation.docker.enable = true;
    environment.systemPackages = [ pkgs.docker-compose ];
}
