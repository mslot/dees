#!/usr/bin/env bash

echo 'installing programs for compiling emacs'
sudo dnf install -y jansson-devel mpfr-devel libmpc-devel gmp-devel libgccjit-devel autoconf texinfo libX11-devel jansson jansson-devel libXpm libXaw-devel \
    libjpeg-turbo-devel libpng-devel giflib-devel libtiff-devel gnutls-devel ncurses-devel gtk3-devel webkit2gtk3-devel

echo 'compiling emacs'
cd ~/dees/external/emacs

echo 'cleaning before beginning'
make clean

echo 'bootstraping'
make bootstrap

./autogen.sh
./configure --with-dbus --with-gif --with-jpeg --with-png --with-rsvg \
    --with-tiff --with-xft --with-xpm --with-gpm=no \
    --with-xwidgets --with-modules --with-native-compilation --with-pgtk \
    -with-threads --with-included-regex

make -j16
sudo make install bindir=~/dees/bin/emacs
