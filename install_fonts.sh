#!/usr/bin/env bash

echo 'installing fonts'
sudo dnf copr enable -y peterwu/iosevka
sudo dnf install -y iosevka-fonts
sudo dnf copr enable elxreno/jetbrains-mono-fonts -y
sudo dnf install jetbrains-mono-fonts -y

echo 'installing Jetbrains Mono nerd font'
git clone https://github.com/ryanoasis/nerd-fonts ~/git/nerd-font
~/git/nerd-font/install.sh Meslo
echo 'font installed'
