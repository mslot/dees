# .bash_profile

# Get the aliases and functions
if [ -f ~/.bashrc ]; then
	. ~/.bashrc
fi

PATH=$PATH:~/.emacs.d/bin
. "$HOME/.cargo/env"
