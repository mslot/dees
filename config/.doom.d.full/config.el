;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;; (map! "C-:" #'avy-goto-char-2)

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!
;;

;; dark title bar
(defun set-selected-frame-dark (&rest _)
  (call-process-shell-command "xprop -f _GTK_THEME_VARIANT 8u -set _GTK_THEME_VARIANT \"dark\" -id \"$(xdotool getactivewindow)\""))

;; for new frames
(add-hook 'after-make-frame-functions #'set-selected-frame-dark)

;; for init frame
(call-process-shell-command "xprop -f _GTK_THEME_VARIANT 8u -set _GTK_THEME_VARIANT \"dark\" -id \"$(xdotool getactivewindow)\"")


;; fancy splash
(setq fancy-splash-image "~/dees/config/.doom.d.full/init.png")

(with-eval-after-load 'doom-themes
  (setq doom-themes-treemacs-theme "doom-colors") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  (doom-themes-org-config))

(after! lsp-ui
  (setq lsp-ui-doc-en t)
  (setq lsp-ui-doc-show-with-cursor t)
  (setq lsp-ui-doc-position 'top)
  (setq lsp-signature-render-documentation nil)
  (setq lsp-ui-doc-delay 3)
  (setq lsp-ui-doc-max-width 60)
  (setq lsp-ui-doc-max-height 800)
  (setq lsp-ui-sideline-enable nil))
;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
;; (setq user-full-name "John Doe"
;;      user-mail-address "john@doe.com")

(setq flycheck-check-syntax-automatically '(mode-enabled save))
;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:

(setq font-size 14)
(when (string= (system-name) "hopper")
  (setq font-size 28))

 (setq doom-font (font-spec :family "Jetbrains Mono" :size font-size :weight 'normal)
       doom-variable-pitch-font (font-spec :family "monospace" :size font-size))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-gruvbox)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")
(after! org
  (setq company-global-modes '(not org-mode))
  (add-to-list 'org-capture-templates
      '(("i" "Inbox" entry (file+headline "~/org/inbox.org" "Inbox")
         "* %?\n  %i\n  %a")
        )))

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.
;;

;; Custom bindings
(map! "C-," #'avy-goto-char-timer)
(map! "C-a" #'treemacs-select-window)

;; We really want rust clippy instead
(setq lsp-rust-analyzer-cargo-watch-command "clippy")

;; emacs everywhere needs this
(setq emacs-everywhere-paste-p t)

;; neckbeard thingy
(after! circe
  (setq company-global-modes '(not circe-channel-mode))

  (setq circe-use-cycle-completion nil)
  (set-company-backend! 'circe-channel-mode nil)
  (set-irc-server! (+pass-get-secret "irc/libera.chat/host")
    `(
      :port ,(+pass-get-secret "irc/libera.chat/port")
      :nick "nick"
      :user ,(+pass-get-user "irc/libera.chat")
      :pass (lambda (&rest _) (+pass-get-secret "irc/libera.chat"))
      :channels ())))

(after! dap-mode
;; rusty ferris
  (dap-register-debug-template "Rust::GDB Run Configuration"
                             (list :type "gdb"
                                   :request "launch"
                                   :name "GDB::Run"
                           :gdbpath "rust-gdb"
                                   :target nil
                                   :cwd nil)))
