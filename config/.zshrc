# We need pyenv
# according to https://stribny.name/blog/install-python-dev/ these two needs to be here
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

# Source Prezto
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...
alias ls='exa --icons --git --header --group-directories-first -l'
alias cls='clear'
alias cat='bat'
alias k='kubectl'
alias e='emacsclient -nw'

eval "$(starship init zsh)"
eval "$(pyenv init -)"
eval "$(_PIPENV_COMPLETE=zsh_source pipenv)"
