#!/usr/bin/env bash

if [ -d "$HOME/.emacs.d" ]; then
    echo "doom emacs already installed"
else
    echo "installing doom emacs"
    git clone --depth 1 https://github.com/doomemacs/doomemacs ~/.emacs.d
    ~/.emacs.d/bin/doom install
fi
