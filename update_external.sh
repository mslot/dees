#!/usr/bin/env bash

# This updates all external modules (emacs etc) to latest commit, so the can be built
git submodule update --recursive
