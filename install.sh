#!/usr/bin/env bash

echo 'installing stow'
sudo dnf install -y stow

echo 'bootstrapping submodules'
git submodule update --init --recursive
git submodule update --remote

echo 'making bin dir'
mkdir -p ~/dees/bin

echo 'going to dees directory'
cd ~/dees

# echo 'creating .emacs.full'
# mkdir -p ~/.emacs.full

. ~/dees/install_emacs.sh
. ~/dees/install_doom_emacs.sh

echo 'cding to external'
cd external/

echo 'stowing full doom emacs to full directory'
stow doom-emacs-full -t ~/.emacs.full

# echo 'removing .emacs.d, and installing chemacs2'
# rm -rf ~/.emacs.d
# git clone https://github.com/plexus/chemacs2.git ~/.emacs.d

. ~/dees/install_config.sh
. ~/dees/install_programs.sh
. ~/dees/install_rust-analyzer.sh
. ~/dees/install_hexchat.sh
. ~/dees/install_fonts.sh

mkdir -p ~/.local/bin

echo 'stowing emacs'
cd ~/dees/bin/emacs
stow . -t ~/.local/bin

# disabled for now
# a good link for this: https://www.emacswiki.org/emacs/EmacsAsDaemon
# echo 'starting emacs daemon'
# mkdir -p ~/.config/systemd/user/default.target.wants
# cp emacs.service ~/.config/systemd/user/default.target.wants
# systemctl enable --user emacs
# systemctl start --user emacs

echo 'stowing rust-analyzer'
cd ~/dees/bin/rust-analyzer
stow . -t ~/.local/bin

echo 'done'
cd ~/dees
