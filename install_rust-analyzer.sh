#!/usr/bin/env bash

echo 'compiling rust-analyzer'
cd ~/dees/external/rust-analyzer
cargo build --release --target-dir ~/dees/bin/rust-analyzer
cd ~/dees/bin/rust-analyzer/release

echo 'copying analyzer to parent directory'
cp rust-analyzer ../
cd ../

echo 'removing unwanted files'
rm -rf debug
rm -rf release
rm CACHEDIR.TAG .rustc_info.json
