#!/usr/bin/env bash

cd ~/dees
echo 'stowing config to ~'
stow config -t ~/

cd ~/dees/external

echo 'stowing prezto'
mkdir -p ~/.zprezto
stow prezto -t ~/.zprezto
