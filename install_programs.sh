#!/usr/bin/env bash

echo 'install programs'
sudo dnf install -y hexchat moby-engine python3 python3-tkinter python3-pip pipenv python3 exa protobuf-compiler gtk4-devel docker-compose piper bat pass util-linux-user starship thunderbird zsh jq shfmt ripgrep fd-find editorconfig multimarkdown curl ShellCheck tidy xwininfo xprop xdotool xclip

echo "setting python up for development"
pip install pytest black pyflakes isort pyright pipenv
pyenv install 3.10.4
pyenv global 3.10.4

echo "installing pyenv"
if [ -d "$HOME/.pyenv" ]; then
    echo "pyenv already installed"
else
    echo "installing pyenv"
    git clone https://github.com/pyenv/pyenv.git ~/.pyenv
fi

echo 'installing rustup and adding src'
if [ -d "$HOME/.cargo" ]; then
    echo ".cargo exists"
else
    echo "installing rustup"
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    . $HOME/.cargo/env
    rustup component add rust-src
fi

echo 'setting zsh to default shell'
chsh -s $(which zsh)
